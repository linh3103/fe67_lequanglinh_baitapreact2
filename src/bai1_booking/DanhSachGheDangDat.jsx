import React, { Component } from 'react';

class DanhSachGheDangDat extends Component {

    renderCartItem = () =>{
        let itemHTML = this.props.danhsachghedangdat.map(ghe => 
            <tr key={ghe.SoGhe}>
                <td>Ghế: {ghe.TenGhe}</td>
                <td>Giá: ${ghe.Gia}</td>
                <td>
                    <span onClick={() => this.props.addOrCancelSeat(ghe)}>
                        Hủy
                    </span>
                </td>
            </tr>
        )
        return itemHTML
    }

    render() {

        let listWaiting = this.props.danhsachghedangdat

        let totalGhe = listWaiting.reduce(total =>{
            return total += 1;
        }, 0);

        let totalGia = listWaiting.reduce((total, ghe) =>{
            return total += ghe.Gia;
        }, 0)
        return (
            <div className="container">
                <h3 className="py-2">Danh sách ghế đang đặt ({totalGhe})</h3>
                <table className="table table-striped">
                    <tbody>
                        {this.renderCartItem()}
                    </tbody>
                    {
                    totalGhe > 0 ?
                        (
                            <tfoot>
                                <tr>
                                    <td>
                                        <span onClick={() => this.props.cancelAll()}>Hủy tất cả</span>
                                    </td>
                                    <td>
                                        Tổng tiền: ${totalGia}
                                    </td>
                                    <td></td>
                                </tr>
                            </tfoot>
                        )
                    :null
                    }
                </table>
            </div>
        );
    }
}

export default DanhSachGheDangDat;