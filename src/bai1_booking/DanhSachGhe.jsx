import React, { Component } from 'react';
import GheItem from './GheItem';

class DanhSachGhe extends Component {
    renderListGhe = () =>{
        const listGheHTML = this.props.danhsachghe.map(ghe =>
            <GheItem key={ghe.SoGhe} ghe = {ghe} addOrCancelSeat={this.props.addOrCancelSeat} 
            danhsachghedangdat={this.props.danhsachghedangdat}/>
        )
        return listGheHTML
    }
    render() {
        return (
            <div className="container border border-primary pt-2 rounded bg-light">
                <h3 className="bg-secondary rounded text-white py-2">Tài xế</h3>
                <div className="row">
                    {this.renderListGhe()}
                </div>
            </div>
        );
    }
}

export default DanhSachGhe;