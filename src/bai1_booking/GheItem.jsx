import React, { Component } from 'react';
import './Ghe.css'
class GheItem extends Component {

    handleAddOrCancel = () =>{
        this.props.addOrCancelSeat(this.props.ghe)
    }

    // Kiểm tra số ghế đầu vào (sogheParam):
    // + Nếu sogheParam có trong danhsachghedangdat thì trả về true
    // + Ngược lại trả về false
    checkGhe = (sogheParam) =>{
        let index = this.props.danhsachghedangdat.findIndex(ghe => ghe.SoGhe === sogheParam);
        if(index !== -1){
            return true
        }
        return false
    }

    renderGhe = (gheParam) =>{
        let gheHTML = "";
        // Kiểm tra trạng thái của ghế đầu vào (gheParam)
        gheParam.TrangThai ? 
        // + Nếu gheParam có trạng thái true thì sẽ có màu đỏ và disabled
        (gheHTML = <button className="btn btn-danger" disabled>{gheParam.SoGhe}</button>):
        // + Nếu gheParam có trạng thái false thì tiếp tục kiểm tra số ghế của gheParam
        (
            this.checkGhe(gheParam.SoGhe) ? 
            // Nếu số ghế của gheParam có trong danhsachghedangdat thì gheParam sẽ có màu xanh
            (gheHTML = <button onClick={this.handleAddOrCancel} className="btn btn-success">{gheParam.SoGhe}</button>):
            // Ngược lại gheParam sẽ trở về màu đen
            (gheHTML = <button onClick={this.handleAddOrCancel} className="btn btn-dark">{gheParam.SoGhe}</button>)
        )
        return gheHTML
    }

    render() {
        const ghe = this.props.ghe
        return (
            <div className="col-3 my-2">
                {this.renderGhe(ghe)}
            </div>
        );
    }
}

export default GheItem;