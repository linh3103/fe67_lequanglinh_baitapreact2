import './App.css';
import HomeBooking from './bai1_booking/HomeBooking';
import HomeGlass from './bai2_trying_glasses/HomeGlass';

function App() {
  return (
    <div className="App">
      <HomeBooking/>
      <HomeGlass/>
    </div>
  );
}

export default App;
