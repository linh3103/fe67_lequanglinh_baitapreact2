import React, { Component } from 'react';
import GlassItem from './GlassItem';

class ListGlasses extends Component {
    renderGlass = () =>{
        let glassHTML = this.props.arrProduct.map(item => 
            <GlassItem key={item.id} glass={item} setSelectedGlass={this.props.setSelectedGlass}/>    
        )
        return glassHTML;
    }
    render() {
        return (
            <div className="container py-4 bg-light rounded border border-dark">
                <div className="list-glass">
                    {this.renderGlass()}
                </div>
            </div>
        );
    }
}

export default ListGlasses;