import React, { Component } from 'react';

class Model extends Component {
    render() {
        const {name, url, desc} = this.props.selectedGlass
        return (
            <div className="container">
                <div className="row">
                    <div className="card-model border border-dark my-5">
                        <img src={url} alt="" width="200px"/>
                        <div className="card-desc px-3">
                            <h4 className="text-left">{name}</h4>
                            <p className="text-left">
                                {desc}
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Model;