import React, { Component } from 'react';
import './Home.css'
import ListGlasses from './ListGlasses';
import Model from './Model';
import v1 from '../assets/images/v1.png'
import v2 from '../assets/images/v2.png'
import v3 from '../assets/images/v3.png'
import v4 from '../assets/images/v4.png'
import v5 from '../assets/images/v5.png'
import v6 from '../assets/images/v6.png'
import v7 from '../assets/images/v7.png'
import v8 from '../assets/images/v8.png'
import v9 from '../assets/images/v9.png'
class HomeGlass extends Component {
    // Products
    arrProduct = [
        {
           "id":1,
           "price":30,
           "name":"GUCCI G8850U",
           "url": v1,
           "desc":"Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. "
        },
        {
           "id":2,
           "price":50,
           "name":"GUCCI G8759H",
           "url": v2,
           "desc":"Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. "
        },
        {
           "id":3,
           "price":30,
           "name":"DIOR D6700HQ",
           "url": v3,
           "desc":"Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. "
        },
        {
           "id":4,
           "price":30,
           "name":"DIOR D6005U",
           "url": v4,
           "desc":"Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. "
        },
        {
           "id":5,
           "price":30,
           "name":"PRADA P8750",
           "url": v5,
           "desc":"Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. "
        },
        {
           "id":6,
           "price":30,
           "name":"PRADA P9700",
           "url": v6,
           "desc":"Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. "
        },
        {
           "id":7,
           "price":30,
           "name":"FENDI F8750",
           "url": v7,
           "desc":"Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. "
        },
        {
           "id":8,
           "price":30,
           "name":"FENDI F8500",
           "url": v8,
           "desc":"Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. "
        },
        {
           "id":9,
           "price":30,
           "name":"FENDI F4300",
           "url": v9,
           "desc":"Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. "
        }
     ];

    constructor(props) {
        super(props);
        this.state={
            selectedGlass: this.arrProduct[0]
        }
    }

    setSelectedGlass = (glass) =>{
        this.setState({
            selectedGlass: glass
        })
    }
    
    render() {
        return (
            <div className="glass py-5 mt-5">
                <h1>TRYING GLASSES ONLINE APP</h1>
                <Model selectedGlass={this.state.selectedGlass}/>
                <ListGlasses arrProduct={this.arrProduct} setSelectedGlass={this.setSelectedGlass}/>
            </div>
        );
    }
}

export default HomeGlass;