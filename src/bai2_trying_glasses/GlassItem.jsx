import React, { Component } from 'react';
class GlassItem extends Component {
    render() {
        const item = this.props.glass;
        return (
            <div onClick={() => this.props.setSelectedGlass(item)} className="glass-item mx-2 rounded">
                <img src={item.url} alt="kính" width="100px"/>
            </div>
        );
    }
}

export default GlassItem;